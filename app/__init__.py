from flask import Flask
from flask_restplus import Api
from config import config
from werkzeug.middleware.proxy_fix import ProxyFix

def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1, x_host=1)
    api = Api(app, title='Órama API Challenge', version='1.0', description='API developed for Órama challenge.', prefix="/api")

    from .controllers.freelancer import api as freelancer_ns
    api.add_namespace(freelancer_ns, path='/freelancer')

    return app